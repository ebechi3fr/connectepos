//
//  Category.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/22/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Category : NSManagedObject

@property (nonatomic, retain) NSString * name;

@end
