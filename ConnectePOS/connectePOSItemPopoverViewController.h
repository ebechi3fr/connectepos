//
//  connectePOSItemPopoverViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/11/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol connectePOSItemPopoverDelegate <NSObject>
- (void)didCancelButtonClicked;
- (void)didSaveButtonClicked;
@end

@interface connectePOSItemPopoverViewController : UIViewController <UITextFieldDelegate>
@property (weak, nonatomic) id<connectePOSItemPopoverDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *itemName;
@property (weak, nonatomic) IBOutlet UITextField *itemCategoryName;
@property (weak, nonatomic) IBOutlet UITextField *itemPrice;
@property (weak, nonatomic) IBOutlet UISwitch *isItemTaxable;
@property (weak, nonatomic) IBOutlet UITextField *itemDescription;
- (IBAction)onCancelClicked:(UIBarButtonItem *)sender;
- (IBAction)onSaveItemClicked:(UIBarButtonItem *)sender;
@end
