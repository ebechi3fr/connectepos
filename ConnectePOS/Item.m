//
//  Item.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/11/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "Item.h"


@implementation Item

@dynamic isTaxable;
@dynamic price;
@dynamic itemCart;

@end
