//
//  connectePOSMethodOfPaymentViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface connectePOSMethodOfPaymentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *transactionCostTableView;
@property (strong, nonatomic) NSNumber *total;
@property (strong, nonatomic) NSNumber *taxes;
@property (strong, nonatomic) NSNumber *subtotal;

- (void)setMenuStateSelected:(int)currentCheckoutMenuState;

@end
