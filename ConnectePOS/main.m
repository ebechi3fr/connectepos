//
//  main.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "connectePOSAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([connectePOSAppDelegate class]));
    }
}
