//
//  connectePOSMasterViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@class connectePOSDetailViewController;

#import <CoreData/CoreData.h>

@interface connectePOSMasterViewController : UITableViewController //<NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) connectePOSDetailViewController *detailViewController;
//@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) NSNumber *currentCheckoutMenuState;
@property (strong, nonatomic) NSNumber *currentRefundMenuState;

- (void)selectTableRow:(NSInteger)r inSection:(NSInteger)s;
- (void)disableTableCell:(UITableViewCell *)cell;
- (void)enableTableCell:(UITableViewCell *)cell;
@end
