//
//  Cart.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/28/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class InventoryItem, Item;

@interface Cart : NSManagedObject

@property (nonatomic, retain) NSNumber * totalCost;
@property (nonatomic, retain) NSSet *cartInventoryItem;
@property (nonatomic, retain) NSSet *cartItem;
@end

@interface Cart (CoreDataGeneratedAccessors)

- (void)addCartInventoryItemObject:(InventoryItem *)value;
- (void)removeCartInventoryItemObject:(InventoryItem *)value;
- (void)addCartInventoryItem:(NSSet *)values;
- (void)removeCartInventoryItem:(NSSet *)values;

- (void)addCartItemObject:(Item *)value;
- (void)removeCartItemObject:(Item *)value;
- (void)addCartItem:(NSSet *)values;
- (void)removeCartItem:(NSSet *)values;

@end
