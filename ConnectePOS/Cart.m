//
//  Cart.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/28/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "Cart.h"
#import "InventoryItem.h"
#import "Item.h"


@implementation Cart

@dynamic totalCost;
@dynamic cartInventoryItem;
@dynamic cartItem;

@end
