//
//  connectePOSCashPaymentViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSCashPaymentViewController.h"
#import "connectePOSMasterViewController.h"
#import "connectePOSAppDelegate.h"
#include "ControlVariables.h"

@interface connectePOSCashPaymentViewController ()
@property (strong, nonatomic) NSNumberFormatter *numberFormatter; //allows numbers to be properly formated for display
@end

@implementation connectePOSCashPaymentViewController
@synthesize transactionTableView = _transactionTableView;
@synthesize exactAmountButton = _exactAmountButton;
@synthesize display = _display;
@synthesize cashOutButton = _cashOutButton;
@synthesize total = _total;
@synthesize changeDue = _changeDue;
@synthesize numberFormatter = _numberFormatter;

NSInteger numberOfRowsInCashPaymentMethodTable = 2;
double currentTender = 0.00;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.changeDue = [[NSNumber alloc] initWithDouble:0.00];
    
    // set the transaction table view datasource and delegate
    self.transactionTableView.delegate = self;
    self.transactionTableView.dataSource = self;
    [self.exactAmountButton setTitle:[NSNumberFormatter localizedStringFromNumber:self.total numberStyle:NSNumberFormatterCurrencyStyle] forState:UIControlStateNormal];
    
    // initialize number formatter used to display numbers in dollar currency
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    [self.numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    // specify positive, zero, and negative formats
    [self.numberFormatter setPositiveFormat:@"$#,##0.00"];
    [self.numberFormatter setNegativeFormat:@"($#,##)0.00"];
    [self.numberFormatter setZeroSymbol:@"$0.00"];
    currentTender = 0.00;
    [self calculateAndDisplayChangeDue];

}

- (void)viewDidUnload
{
    [self setDisplay:nil];
    [self setTransactionTableView:nil];
    [self setExactAmountButton:nil];
    [self setCashOutButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    // select current checkout menu state on the master view
    [self setMenuStateSelected:CHECKOUT_METHOD_OF_PAYMENT_STATE];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)setMenuStateSelected:(int)currentCheckoutMenuState
{
    // Get the master view controller in order to access the checkout menu state
    UIWindow *window = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] window];
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
    connectePOSMasterViewController *controller = (connectePOSMasterViewController *)masterNavigationController.topViewController;
    
    //set the current checkout menu state to the checkout initial state
    controller.currentCheckoutMenuState = [NSNumber numberWithInteger:currentCheckoutMenuState];
    //update the checkout menu state
    [controller.tableView reloadData];
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return DEFAULT_NUM_TABLE_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfRowsInCashPaymentMethodTable;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    UITableViewCell *cell;
    
    cellIdentifier = @"cashPaymentMethodCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        //            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    UILabel *transactionLabel = (UILabel *)[cell viewWithTag:SUBTOTAL_TAX_TAG];
    if ([indexPath row] == 0) { //Subtotal row
        transactionLabel.text = @"Total";
        cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:self.total numberStyle:NSNumberFormatterCurrencyStyle];
    } else {
        transactionLabel.text = @"Change Due";
        if (self.changeDue.doubleValue < 0) {
            cell.textLabel.textColor = [UIColor redColor];
        } else {
            cell.textLabel.textColor = [UIColor blackColor];
        }
        cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:self.changeDue numberStyle:NSNumberFormatterCurrencyStyle];
    }
    
    [cell.textLabel setTextAlignment:UITextAlignmentRight];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    return cell;
}

#pragma mark - Calculation Controlls

- (IBAction)digitPressed:(UIButton *)sender {
    NSString *digit = [sender currentTitle];
    NSLog(@"digit pressed: %@", digit);
    // Convert the digit pressed into cents
    double digitInCents = [digit doubleValue] * UNIT_IN_CENTS;
    
    // get the current number enterred so far from the display
    currentTender = [[self.numberFormatter numberFromString:self.display.text] doubleValue];//[self.display.text doubleValue];
//    currentTender = [[self.numberFormatter numberFromString:[self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text] doubleValue]; //gets the textlabel of the selected row
    //[[self.numberFormatter numberFromString:self.display.text] doubleValue];//[self.display.text doubleValue];
    
    
    // shift running total decimal to the right
    //    runningTotal *= DECIMAL_SHIFTER_CONST;
    currentTender *= DECIMAL_SHIFTER_CONST;
    
    if ([digit isEqualToString:@"00"]) {
        //        runningTotal *= DECIMAL_SHIFTER_CONST;
        currentTender *= DECIMAL_SHIFTER_CONST;
    }
    
    // add the digit pressed to the running total
    //    runningTotal += digitInCents;
    currentTender += digitInCents;
    
//    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentTender] numberStyle:NSNumberFormatterCurrencyStyle];
     self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentTender] numberStyle:NSNumberFormatterCurrencyStyle];//[NSString stringWithFormat: @"%f", runningTotal];
    [self calculateAndDisplayChangeDue];
}

- (void)addToCurrentTransaction:(double)itemPrice
{
    if (itemPrice == 0.0) {
        return;
    }
    
    currentTender += itemPrice;
}

- (IBAction)deletePressed {
    currentTender = [[self.numberFormatter numberFromString:self.display.text] doubleValue]; //gets the textlabel of the selected row
    
    // shift current number decimal to the left
    NSNumberFormatter *deleteFormatter = [[NSNumberFormatter alloc] init];
    [deleteFormatter setRoundingMode:NSNumberFormatterRoundDown];
    [deleteFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [deleteFormatter setPositiveFormat:@"$#,##0.00"];
    [deleteFormatter setNegativeFormat:@"($#,##)0.00"];
    [deleteFormatter setZeroSymbol:@"$0.00"];
    
    currentTender /= DECIMAL_SHIFTER_CONST;
    NSLog(@"delete: %f", currentTender);
    if (currentTender < 0.01) {
        currentTender = 0.0;
        NSLog(@"delete after format: %f", currentTender);
    }
    self.display.text = [deleteFormatter stringFromNumber:[NSNumber numberWithDouble:currentTender]];
    [self calculateAndDisplayChangeDue];
}

- (IBAction)addPressed:(UIButton *)sender {
    //gets the textlabel of the tender
    double amountPressed = [[self.numberFormatter numberFromString:sender.titleLabel.text] doubleValue];
    if (amountPressed == 0.0) {
        return;
    }
    [self addToCurrentTransaction:amountPressed];
            
    NSLog(@"add: %f", currentTender);
    // update the display
    self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentTender] numberStyle:NSNumberFormatterCurrencyStyle];
    [self calculateAndDisplayChangeDue];
}

- (void)calculateAndDisplayChangeDue
{
    self.changeDue = [NSNumber numberWithDouble:(currentTender - self.total.doubleValue)];
    if ((self.changeDue.doubleValue > -0.01) && (self.changeDue.doubleValue < 0.01)) {
        self.changeDue = [NSNumber numberWithDouble:0.0];
    }
    NSLog(@"Change due: %f", self.changeDue.doubleValue);
    [self.transactionTableView reloadData];
    if (self.changeDue.doubleValue >= 0.0) {
        self.cashOutButton.enabled = YES;
    } else {
        self.cashOutButton.enabled = NO;
    }
}

@end
