//
//  connectePOSAppDelegate.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface connectePOSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
