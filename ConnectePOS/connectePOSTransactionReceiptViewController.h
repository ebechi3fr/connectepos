//
//  connectePOSTransactionReceiptViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/8/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface connectePOSTransactionReceiptViewController : UIViewController

- (void)setMenuStateSelected:(int)currentCheckoutMenuState;

@end
