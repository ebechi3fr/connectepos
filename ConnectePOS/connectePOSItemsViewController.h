//
//  connectePOSItemsViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/4/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "connectePOSItemPopoverViewController.h"
#import "InventoryItem.h"
#import "Category.h"

@class connectePOSItemsViewController;

@protocol connectePOSItemsViewControllerDelegate <NSObject>
@optional
- (void)updateCart:(connectePOSItemsViewController *)sender With:(NSMutableDictionary *)selectedInventoryItems from:(NSArray *)inventoryItems;
@end

@interface connectePOSItemsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate,connectePOSItemPopoverDelegate>
@property (weak, nonatomic) id<connectePOSItemsViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *categoriesTableview;
@property (weak, nonatomic) IBOutlet UIImageView *categoriesTableviewFrame;
@property (weak, nonatomic) IBOutlet UITableView *inventoryItemsTableview;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectMethodOfPayment;
@property (weak, nonatomic) IBOutlet UISearchBar *itemSearchBar;
@property (weak, nonatomic) connectePOSItemPopoverViewController *itemPopoverController; //might not need it but leave it for now
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UILabel *subtotalLabel;
@property (strong, nonatomic) NSNumber *itemsSubtotal;
@property (strong, nonatomic) NSNumber *currentSubtotal;
@property (strong, nonatomic) NSMutableDictionary* currentItemSelection; //used to sync subtotal between storefront and items view

- (int)array:(NSArray*)arrayObject containsString:(NSString *)str; //checks if str is contained in arrayObject
- (void)updateSubtotalLabel;
- (void)filterContentForSearchText:(NSString *)searchText;
- (void)setMenuStateSelected:(int)currentCheckoutMenuState;

@end
