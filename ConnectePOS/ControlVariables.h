//
//  ControlVariables.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/10/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#ifndef ConnectePOS_ControlVariables_h
#define ConnectePOS_ControlVariables_h

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// iPad connectePOS CONSTANTS
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//connectePOS System Main menu

// Default Menu state is "Checkout" menu
// States for Main Menu
#define DEFAULT_MENU_STATE 0
#define CHECKOUT_MENU_STATE 0
#define REFUND_MENU_STATE 1
#define SETTINGS_MENU_STATE 2

// States for "Checkout" menu
#define CHECKOUT_START_STATE 0
#define CHECKOUT_METHOD_OF_PAYMENT_STATE 1
#define CHECKOUT_END_STATE 2

// States for "Refund" menu
#define REFUND_START_STATE 0
#define REFUND_END_STATE 2
////////////////////////////////


//connectePOS System Store front

// the unit for any amount entered is 1 cents
#define UNIT_IN_CENTS 0.01

// used to shift decimal point to the right or left.
// multiply to shift right
// divide to shift left
#define DECIMAL_SHIFTER_CONST 10

// the Michigan tax amount in percentage
#define MICHIGAN_TAX 0.06

// the number of rows for transaction cost table (tax and subtotal)
#define NUM_ROWS_DURING_TRANSACTION 3//2
#define INITIAL_NUM_ROWS 0

// Cell tags for the Cart table
#define ITEM_NAME_TAG 1
#define ITEM_DESCRIPTION_TAG 2
#define SUBTOTAL_TAX_TAG 1
/////////////////////////////////

//connectePOS Inventory items

// Cell tags for the item inventory table
#define ITEM_ADDTOCART_BUTTON_TAG 3
#define ITEM_STEPPER_BUTTON_TAG 4
#define ITEM_QUANTITY_TAG 5
#define ITEM_PRICE_TOTAL_TAG 6
#define CROSS_LABEL_TAG 7
/////////////////////////////////////////

// the default number of sections in every table through the app
#define DEFAULT_NUM_TABLE_SECTIONS 1
////////////////
#endif
