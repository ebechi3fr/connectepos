//
//  connectePOSMasterViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSMasterViewController.h"
#import "connectePOSDetailViewController.h"
#include "ControlVariables.h"


@interface connectePOSMasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@property (strong, nonatomic) NSArray *checkoutMenuList;
@property (strong, nonatomic) NSArray *refundMenuList;
@property (strong, nonatomic) NSArray *settingsMenuList;
@property (strong, nonatomic) NSMutableArray *mainMenuList;
//@property (strong, nonatomic) NSArray *segmentedItems;
@property (strong, nonatomic) NSArray *mainMenuStates;
@property (strong, nonatomic) NSString *currentMenuState;
@property (strong, nonatomic) UISegmentedControl *mainMenuControl;
@end


@implementation connectePOSMasterViewController

@synthesize detailViewController = _detailViewController;
//@synthesize fetchedResultsController = __fetchedResultsController;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize checkoutMenuList = _checkoutMenuList;
//@synthesize segmentedItems = _segmentedItems;
@synthesize refundMenuList = _refundMenuList;
@synthesize settingsMenuList = _settingsMenuList;
@synthesize mainMenuStates = _mainMenuStates;
@synthesize currentMenuState = _currentMenuState;
@synthesize mainMenuControl = _mainMenuControl;
@synthesize mainMenuList = _mainMenuList;
@synthesize currentRefundMenuState = _currentRefundMenuState;
@synthesize currentCheckoutMenuState = _currentCheckoutMenuState;

//NSInteger currentCheckoutMenuState = CHECKOUT_START_STATE;
//NSInteger currentRefundMenuState = REFUND_START_STATE;

- (void)awakeFromNib
{
    self.clearsSelectionOnViewWillAppear = NO;
    self.contentSizeForViewInPopover = CGSizeMake(320.0, 600.0);
    [super awakeFromNib];
}



- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.leftBarButtonItem = self.editButtonItem;
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(insertNewObject:)];
//    self.navigationItem.rightBarButtonItem = addButton;
    self.detailViewController = (connectePOSDetailViewController *)[[self.splitViewController.viewControllers lastObject] topViewController];
    
    // Inintialize arrays used for populating menu list. 
    // Note: needs to be redone using String files    
    self.checkoutMenuList = [[NSArray alloc] 
                       initWithObjects:@"Ring up items", 
                       @"Select Method of Payment", @"Send Receipt", nil];
    self.refundMenuList = [[NSArray alloc] initWithObjects:@"Select Transaction",
                           @"Select Items to Refund", @"Send Refund Receipt", nil];
    self.settingsMenuList = [[NSArray alloc] initWithObjects:@"Tax",
                             @"Set Tips", @"Signature", @"Change Store", @"Transaction History", @"Inventory Items", @"Cashier Login", @"Help", @"About Connecte POS", nil];
    self.mainMenuStates = [[NSArray alloc] initWithObjects:@"Checkout", @"Refund", @"Settings", nil];

    self.currentCheckoutMenuState = [NSNumber numberWithInt:CHECKOUT_START_STATE];
    self.currentRefundMenuState = [NSNumber numberWithInt:REFUND_START_STATE];
    // Set the current state of the main menu, the title of the current and detailed view to be the default state
    self.currentMenuState = [self.mainMenuStates objectAtIndex:DEFAULT_MENU_STATE];
    self.title = self.currentMenuState;
    self.detailViewController.title = [self.checkoutMenuList objectAtIndex:self.currentCheckoutMenuState.integerValue];
    self.mainMenuList = [self.checkoutMenuList mutableCopy];
    
    // Create and place main menu control on at the center of the bottom bar
    self.mainMenuControl = [[UISegmentedControl alloc] initWithItems:self.mainMenuStates];
    self.mainMenuControl.segmentedControlStyle = UISegmentedControlStyleBar;
    self.mainMenuControl.selectedSegmentIndex = DEFAULT_MENU_STATE;    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:self.mainMenuControl];    
    [self.mainMenuControl addTarget:self action:@selector(segmentedControlIndexChanged) //Error is Here
               forControlEvents:UIControlEventValueChanged];
    [self.mainMenuControl sizeToFit];
    NSString *space = @"      ";
    UIBarButtonItem *flexSpace = [[UIBarButtonItem alloc] initWithTitle:space style:UIBarButtonItemStylePlain target:nil action:nil];
    flexSpace.enabled = NO;
    NSArray *theToolbarItems = [NSArray arrayWithObjects:flexSpace, item, nil];
    [self setToolbarItems:theToolbarItems];
    [self selectTableRow:0 inSection:0]; //set the first row in the main menu selected to be selected by default
}

- (void)selectTableRow:(NSInteger)r inSection:(NSInteger)s
{   //sets the first row in the table menu to be selected
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s]  animated:NO scrollPosition:UITableViewScrollPositionNone]; 
//    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)disableTableCell:(UITableViewCell *)cell
{
    cell.selected = NO;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.userInteractionEnabled = NO;
    cell.textLabel.enabled = NO;
}

- (void)enableTableCell:(UITableViewCell *)cell
{
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.userInteractionEnabled = YES;
    cell.textLabel.enabled = YES;
}

- (IBAction)segmentedControlIndexChanged{
    NSUInteger selectedMenu = self.mainMenuControl.selectedSegmentIndex;
    self.currentMenuState = [self.mainMenuStates objectAtIndex:selectedMenu];
    self.title = self.currentMenuState;
    [self.mainMenuList removeAllObjects];
    switch (selectedMenu) {
        case CHECKOUT_MENU_STATE:
            NSLog(@"Checkout");
            self.mainMenuList = [self.checkoutMenuList mutableCopy];
            [self.tableView setUserInteractionEnabled:NO];
            [self.tableView reloadData]; //Update the tableview after changing the menu
            [self selectTableRow:self.currentCheckoutMenuState.integerValue inSection:0]; //set the first row in the main menu selected to be selected by default
            self.detailViewController.title = [self.checkoutMenuList objectAtIndex:self.currentCheckoutMenuState.integerValue];
            break;
        case REFUND_MENU_STATE:
            NSLog(@"Refund");
            self.mainMenuList = [self.refundMenuList mutableCopy];
            [self.tableView setUserInteractionEnabled:NO];
            [self.tableView reloadData]; //Update the tableview after changing the menu
            [self selectTableRow:self.currentRefundMenuState.integerValue inSection:0]; //set the first row in the main menu selected to be selected by default
            self.detailViewController.title = [self.refundMenuList objectAtIndex:self.currentRefundMenuState.integerValue];
         break;
        case SETTINGS_MENU_STATE:
            NSLog(@"Setting");
            self.mainMenuList = [self.settingsMenuList mutableCopy];
            [self.tableView setUserInteractionEnabled:YES];
//            [self selectTableRow:0 inSection:0]; //set the first row in the main menu selected to be selected by default
            [self.tableView reloadData]; //Update the tableview after changing the menu
           break;
        default:
            break;
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)insertNewObject:(id)sender
{
//    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
//    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
//    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
//    [newManagedObject setValue:[NSDate date] forKey:@"timeStamp"];
    
    // Save the context.
//    NSError *error = nil;
//    if (![context save:&error]) {
         // Replace this implementation with code to handle the error appropriately.
         // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//        abort();
//    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
//    return [[self.fetchedResultsController sections] count];
    return DEFAULT_NUM_TABLE_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
//    return [sectionInfo numberOfObjects];
    return [self.mainMenuList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    [self configureCell:cell atIndexPath:indexPath];
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    cell.textLabel.text = [self.mainMenuList objectAtIndex:[indexPath row]];
//    if (indexPath.row == 1) {
//        [self disableTableCell:cell];
//        NSLog(@"Setting 1 cell to be selected");
//    }
    NSUInteger selectedMenu = self.mainMenuControl.selectedSegmentIndex;
    switch (selectedMenu) {
        case CHECKOUT_MENU_STATE:
            if ([indexPath row] > self.currentCheckoutMenuState.integerValue) {
                [self disableTableCell:cell];
            } else {
                [self enableTableCell:cell];
                if ([indexPath row] == self.currentCheckoutMenuState.integerValue) {
                    [self selectTableRow:indexPath.row inSection:0];
                }
            }
            break;
        case REFUND_MENU_STATE:
            if ([indexPath row] > self.currentRefundMenuState.integerValue) {
                [self disableTableCell:cell];
            } else {
                [self enableTableCell:cell];
                if ([indexPath row] == self.currentRefundMenuState.integerValue) {
                    [self selectTableRow:indexPath.row inSection:0];
                }
            }
            break;
        case SETTINGS_MENU_STATE:
            [self enableTableCell:cell];
            break;
        default:
            break;
    }
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
//        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
//        
//        NSError *error = nil;
//        if (![context save:&error]) {
//             // Replace this implementation with code to handle the error appropriately.
//             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
//    }   
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
//    self.detailViewController.detailItem = object;
    NSUInteger selectedMenu = self.mainMenuControl.selectedSegmentIndex;
    switch (selectedMenu) {
        case CHECKOUT_MENU_STATE:
            self.detailViewController.title = [self.checkoutMenuList objectAtIndex:self.currentCheckoutMenuState.integerValue];
            break;
        case REFUND_MENU_STATE:
            self.detailViewController.title = [self.refundMenuList objectAtIndex:self.currentRefundMenuState.integerValue];
           break;
        case SETTINGS_MENU_STATE:
            self.detailViewController.title = [self.settingsMenuList objectAtIndex:[indexPath row]];
          break;
        default:
            break;
    }
}

#pragma mark - Fetched results controller

//- (NSFetchedResultsController *)fetchedResultsController
//{
//    if (__fetchedResultsController != nil) {
//        return __fetchedResultsController;
//    }
//    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//    // Edit the entity name as appropriate.
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entity];
//    
//    // Set the batch size to a suitable number.
//    [fetchRequest setFetchBatchSize:20];
//    
//    // Edit the sort key as appropriate.
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeStamp" ascending:NO];
//    NSArray *sortDescriptors = [NSArray arrayWithObjects:sortDescriptor, nil];
//    
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    
//    // Edit the section name key path and cache name if appropriate.
//    // nil for section name key path means "no sections".
//    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Master"];
//    aFetchedResultsController.delegate = self;
//    self.fetchedResultsController = aFetchedResultsController;
//    
//	NSError *error = nil;
//	if (![self.fetchedResultsController performFetch:&error]) {
//	     // Replace this implementation with code to handle the error appropriately.
//	     // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
//	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//	    abort();
//	}
//    
//    return __fetchedResultsController;
//}    

//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
//{
//    [self.tableView beginUpdates];
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
//           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
//{
//    switch(type) {
//        case NSFetchedResultsChangeInsert:
//            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
//}
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
//       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
//      newIndexPath:(NSIndexPath *)newIndexPath
//{
//    UITableView *tableView = self.tableView;
//    
//    switch(type) {
//        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
//}
//
//- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
//{
//    [self.tableView endUpdates];
//}

/*
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed. 
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    // In the simplest, most efficient, case, reload the table view.
    [self.tableView reloadData];
}
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
//    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
//    cell.textLabel.text = [[object valueForKey:@"timeStamp"] description];
    
}

@end
