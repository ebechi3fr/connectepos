//
//  connectePOSItemPopoverViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/11/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSItemPopoverViewController.h"

@interface connectePOSItemPopoverViewController ()

@end

@implementation connectePOSItemPopoverViewController
@synthesize itemName = _itemName;
@synthesize itemCategoryName = _itemCategoryName;
@synthesize itemPrice = _itemPrice;
@synthesize isItemTaxable = _isItemTaxable;
@synthesize itemDescription = _itemDescription;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.itemPrice.delegate = self;
}

- (void)viewDidUnload
{
    [self setItemName:nil];
    [self setItemCategoryName:nil];
    [self setItemPrice:nil];
    [self setIsItemTaxable:nil];
    [self setItemDescription:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)onCancelClicked:(UIBarButtonItem *)sender {
    [self.delegate didCancelButtonClicked];
}

- (IBAction)onSaveItemClicked:(UIBarButtonItem *)sender {
    [self.delegate didSaveButtonClicked];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    text = [text stringByReplacingOccurrencesOfString:@"." withString:@""];
    double number = [text intValue] * 0.01;
    textField.text = [NSString stringWithFormat:@"%.2lf", number];
//    textField.text = [@"$" stringByAppendingString:textField.text];
    return NO;
}

@end
