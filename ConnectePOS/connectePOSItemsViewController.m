//
//  connectePOSItemsViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/4/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSItemsViewController.h"
#import "connectePOSMasterViewController.h"
#import "connectePOSMethodOfPaymentViewController.h"
#include <QuartzCore/QuartzCore.h>
#include "ControlVariables.h"
#import "connectePOSAppDelegate.h"

@interface connectePOSItemsViewController ()
@property (strong, nonatomic) NSMutableArray* categoryArray;
@property (strong, nonatomic) NSMutableArray* inventoryItemArray; //All the inventory items
@property (strong, nonatomic) NSMutableArray* currentDisplayedInventoryItemArray; //Iventory items currently being displayed in the table
@property (weak, nonatomic) UIPopoverController* currentPopover;
@end

@implementation connectePOSItemsViewController
@synthesize categoriesTableview = _categoriesTableview;
@synthesize categoriesTableviewFrame = _categoriesTableviewFrame;
@synthesize inventoryItemsTableview = _inventoryItemsTableview;
@synthesize selectMethodOfPayment = _selectMethodOfPayment;
@synthesize itemSearchBar = _itemSearchBar;
@synthesize categoryArray = _categoryArray;
@synthesize itemPopoverController = _itemPopoverController; //might not need it
@synthesize currentPopover = _currentPopover;
@synthesize inventoryItemArray = _inventoryItemArray;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize subtotalLabel = _subtotalLabel;
@synthesize currentDisplayedInventoryItemArray = _currentDisplayedInventoryItemArray;
@synthesize itemsSubtotal = _itemsSubtotal;
@synthesize currentSubtotal = _currentSubtotal;

bool clearAllSelectionData = NO; //indicates if the clear all selection button has been clicked
NSString *defaultCategoryName = @"All";
NSString *currentDisplayedCategory = @"All";
NSString *removeAllString = @"Remove all";
NSString *addToCartString = @"Add to cart";
NSString *crossLabelString = @"x";

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//    self.itemPopoverController = [[connectePOSItemPopoverViewController alloc] init];
    self.categoriesTableview.delegate = self;
    self.categoriesTableview.dataSource = self;
    self.inventoryItemsTableview.delegate = self;
    self.inventoryItemsTableview.dataSource = self;
    self.itemSearchBar.delegate = self;
        
//    self.itemPopoverController.delegate = self;
    self.categoryArray = [[NSMutableArray alloc] init];
    [self.categoryArray addObject:defaultCategoryName];
//    [self.categoryArray addObject:@"Add new category"]; //replace this with an Add item image icon or button
    
    self.inventoryItemArray = [[NSMutableArray alloc] init];
    self.currentDisplayedInventoryItemArray = [[NSMutableArray alloc] init];
//    self.currentItemSelection = [[NSMutableDictionary alloc] init];
//    self.subtotal = [[NSNumber alloc] initWithDouble:0.0];

    // update the subtotal label
    self.subtotalLabel.text = [NSNumberFormatter localizedStringFromNumber:self.currentSubtotal numberStyle:NSNumberFormatterCurrencyStyle];
    
    //deactivate or activate select method of payment button based on the value of the current subtotal label
    if (self.currentSubtotal.doubleValue == 0.0) {
        self.selectMethodOfPayment.enabled = NO;
    } else {
        self.selectMethodOfPayment.enabled = YES;
    }
    
    // Configure category tableview
    CGRect rect = self.view.frame;
    rect.size.width = self.categoriesTableview.frame.size.height;
    rect.size.height = self.categoriesTableview.frame.size.width+10;
    rect.origin.x = 5;
    rect.origin.y = 100;
    [self.categoriesTableview setTransform:CGAffineTransformMakeRotation(-M_PI/2)]; //rotate the table counter clockwise
    self.categoriesTableview.frame = rect;
    self.categoriesTableview.rowHeight = 200; //size of each cells in table
    NSLog(@"width: %f",self.categoriesTableview.frame.size.width);
    NSLog(@"height: %f",self.categoriesTableview.frame.size.height);
    if (self.managedObjectContext == nil) {
        self.managedObjectContext = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
        //        NSLog(@"After managedObjectContext: %@",  managedObjectContext);
    }
    // get saved Items and categories if any
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Event" inManagedObjectContext:managedObjectContext];
    NSEntityDescription *inventoryItem = [NSEntityDescription entityForName:@"InventoryItem" inManagedObjectContext:self.managedObjectContext];
    NSEntityDescription *itemCategory = [NSEntityDescription entityForName:@"Category" inManagedObjectContext:self.managedObjectContext];

    //Fetch "All Inventory Item" request
    [request setEntity:inventoryItem];
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        // Handle the error.
    }
    self.inventoryItemArray = [mutableFetchResults mutableCopy];
    self.currentDisplayedInventoryItemArray = [mutableFetchResults mutableCopy];
    
    //Fetch Category request
    [request setEntity:itemCategory];
    mutableFetchResults = [[self.managedObjectContext executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        // Handle the error.
    }
    
    for (int i=0; i < [mutableFetchResults count]; i++) {
        Category *tempCategory = [mutableFetchResults objectAtIndex:i];
        [self.categoryArray addObject:tempCategory.name];
//        [self.categoryArray insertObject:tempCategory.name atIndex:[self.categoryArray indexOfObject:self.categoryArray.lastObject]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft) ||
        ([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight)) {
//        set up your landscape view here
        CGRect rect = self.view.frame;
        rect.size.width = self.categoriesTableview.frame.size.width;
        rect.size.height = 79;
        rect.origin.x = 5;
        rect.origin.y = 66;
        self.categoriesTableview.frame = rect;
        NSLog(@"landscape mode");
    } else {
//        set up your portrait view here
        CGRect rect = self.view.frame;
        rect.size.width = self.categoriesTableview.frame.size.width;
        rect.size.height = 79;
        rect.origin.x = 5;
        rect.origin.y = 66;
        self.categoriesTableview.frame = rect;
        NSLog(@"portrait mode");
   }
    // select current checkout menu state on the master view
    [self setMenuStateSelected:CHECKOUT_START_STATE];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.delegate updateCart:self With:self.currentItemSelection from:self.inventoryItemArray];
}

- (void)viewDidUnload
{
    [self setCategoriesTableview:nil];
    [self setCategoriesTableviewFrame:nil];
    [self setCategoriesTableview:nil];
    [self setInventoryItemsTableview:nil];
    [self setSubtotalLabel:nil];
    [self setSelectMethodOfPayment:nil];
    [self setItemSearchBar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.categoryArray = nil;
    self.inventoryItemArray = nil;
    self.currentDisplayedInventoryItemArray = nil;
    self.currentItemSelection = nil;
    self.itemsSubtotal = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
//    [self.categoriesTableview reloadData];
    NSLog(@"origin x: %f", self.categoriesTableview.frame.origin.x);
    NSLog(@"origin y: %f", self.categoriesTableview.frame.origin.y);
    NSLog(@"origin width: %f", self.categoriesTableview.frame.size.width);
    NSLog(@"origin height: %f", self.categoriesTableview.frame.size.height);
   
    if ((fromInterfaceOrientation == UIInterfaceOrientationLandscapeLeft) ||
        (fromInterfaceOrientation == UIInterfaceOrientationLandscapeRight)) { //in portrait mode
        CGRect rect = self.view.frame;
        rect.size.width = self.categoriesTableview.frame.size.width;
        rect.size.height = 79;//self.categoriesTableview.frame.size.height-30;
        rect.origin.x = 5;
        rect.origin.y = 66;
        self.categoriesTableview.frame = rect;//self.categoriesTableviewFrame.frame;
        NSLog(@"portrait mode");
    } else { // in landscape mode
        CGRect rect = self.view.frame;
        rect.size.width = self.categoriesTableview.frame.size.width;
        rect.size.height = 79;//self.categoriesTableview.frame.size.height+30;
        rect.origin.x = 5;
        rect.origin.y = 66;
        self.categoriesTableview.frame = rect;//self.categoriesTableviewFrame.frame;
        NSLog(@"landscape mode");
    }
}

#pragma mark - Popover view

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
        // Dismiss current popover, set new popover
        if ([self.currentPopover isPopoverVisible]) {
            [self.currentPopover dismissPopoverAnimated:YES];
        }
        self.currentPopover = [(UIStoryboardPopoverSegue *)segue popoverController];
        if ([segue.identifier isEqualToString:@"ItemPopoverControllerSegue"]) {
            self.itemPopoverController = (connectePOSItemPopoverViewController *)[self.currentPopover contentViewController];
            self.itemPopoverController.delegate = self;
        }
    } else if ([segue isKindOfClass:[UIStoryboardSegue class]]) {
        if ([segue.identifier isEqualToString:@"ItemsViewControllerSegue"]) {
            NSLog(@"Storyboard segue started");
        } else if ([segue.identifier isEqualToString:@"SelectPaymentFromInventoryViewControllerSegue"]) {
            connectePOSMethodOfPaymentViewController *vc = (connectePOSMethodOfPaymentViewController *) segue.destinationViewController;
            vc.subtotal = self.currentSubtotal;
            // calculate the current transaction tax
            double currentTaxes = self.currentSubtotal.doubleValue * MICHIGAN_TAX;
                        
            // calculate the running total
            double runningTotal = self.currentSubtotal.doubleValue + currentTaxes;

            vc.taxes = [[NSNumber alloc] initWithDouble:currentTaxes];
            vc.total = [[NSNumber alloc] initWithDouble:runningTotal];
       }
    }
}

- (void)didCancelButtonClicked
{
    self.itemPopoverController = nil;
    [self.currentPopover dismissPopoverAnimated:YES];    
}

- (int)array:(NSArray*)arrayObject containsString:(NSString *)str
{
    for (int i = 0; i < arrayObject.count; i++) {
        InventoryItem *item = (InventoryItem *)[arrayObject objectAtIndex:i];
        if ([item.name isEqualToString:str]) {
            return i;
        }
    }
    return -1;
}

- (void)didSaveButtonClicked
{
    BOOL error = NO;
    NSString *errorMessage;
    if ([[self.itemPopoverController.itemName text] isEqualToString:@""]) {
        errorMessage = @"Name is required to save this new Item.\n\n";
        error = YES;
    } else if ([self array:self.inventoryItemArray containsString:self.itemPopoverController.itemName.text] >= 0) {
        errorMessage = @"Name you entered already exist, please enter a different name.\n\n";
        error = YES;
    }
    if ([[self.itemPopoverController.itemPrice text] isEqualToString:@"0.00"]) {
        errorMessage = [errorMessage stringByAppendingString:@"Price is required to save this new Item."];
        error = YES;
    }
    
    if (error) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Invalid Entry!"
                                                          message:errorMessage
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    } else {
        // save the data collected for the new item
        // Create and configure a new instance of the InventoryItem entity.
        InventoryItem *inventoryItem = (InventoryItem *)[NSEntityDescription insertNewObjectForEntityForName:@"InventoryItem" inManagedObjectContext:self.managedObjectContext];
        [inventoryItem setName:self.itemPopoverController.itemName.text];
        if ([self.itemPopoverController.itemCategoryName.text isEqualToString:@""]) {
            [inventoryItem setCategoryName:defaultCategoryName];
        } else {
            [inventoryItem setCategoryName:self.itemPopoverController.itemCategoryName.text];
        }
        if ([self.itemPopoverController.itemDescription.text isEqualToString:@""]) {
            [inventoryItem setItemDescription:@"Item description..."];
        } else {
            [inventoryItem setItemDescription:self.itemPopoverController.itemDescription.text];
        }
        [inventoryItem setPrice:[NSNumber numberWithDouble:[self.itemPopoverController.itemPrice.text doubleValue]]];
        [inventoryItem setIsTaxable:[NSNumber numberWithBool:self.itemPopoverController.isItemTaxable.on]];
        [self.inventoryItemArray addObject:inventoryItem];
        if ([currentDisplayedCategory isEqualToString:inventoryItem.categoryName] ||
            [currentDisplayedCategory isEqualToString:defaultCategoryName]) {
            [self.currentDisplayedInventoryItemArray addObject:inventoryItem];
        }
        if (![inventoryItem.categoryName isEqualToString:@""] && ![self.categoryArray containsObject:inventoryItem.categoryName]) {
            Category *itemCategory = (Category *)[NSEntityDescription insertNewObjectForEntityForName:@"Category" inManagedObjectContext:self.managedObjectContext];
            [itemCategory setName:inventoryItem.categoryName];
            [self.categoryArray insertObject:itemCategory.name atIndex:[self.categoryArray indexOfObject:self.categoryArray.lastObject]];
        }
        NSError *error = nil;
        // save the Item and his category
        if (![self.managedObjectContext save:&error]) {
            // Handle the error.
        }
        [self.currentPopover dismissPopoverAnimated:YES];
        [self.inventoryItemsTableview reloadData];
        [self.categoriesTableview reloadData];
    }
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return DEFAULT_NUM_TABLE_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.categoriesTableview) {
        return [self.categoryArray count];
    } else if (tableView == self.inventoryItemsTableview) {
        return [self.currentDisplayedInventoryItemArray count];//[self.inventoryItemArray count];
    } else {
        return INITIAL_NUM_ROWS;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (tableView == self.inventoryItemsTableview) {
        NSString *tableHeader;
        NSString *string1 = @"Inventory Items";
        NSString *headerTextPositionRight = [string1 stringByPaddingToLength:string1.length + 90 withString:@" " startingAtIndex:0];
        tableHeader = [headerTextPositionRight stringByAppendingString:@"Unit Price"];
        return tableHeader;
    } else {
        return nil;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    static NSString *CellIdentifier; 
    UITableViewCell *cell;
    if (tableView == self.categoriesTableview) {
        CellIdentifier = @"CategoryCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [self.categoryArray objectAtIndex:[indexPath row]];
        [cell.textLabel setTextAlignment:UITextAlignmentCenter];
        [cell.textLabel setTransform:CGAffineTransformMakeRotation(M_PI/2)]; //rotate the cell clockwise to match table rotation
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        [cell.textLabel setFont:[UIFont boldSystemFontOfSize:20]];
        cell.textLabel.numberOfLines = 2;
        [cell.textLabel sizeToFit];
    } else if (tableView == self.inventoryItemsTableview) {
        CellIdentifier = @"InventoryItemCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        UILabel *itemName = (UILabel *)[cell viewWithTag:ITEM_NAME_TAG];
        UILabel *itemDescription = (UILabel *)[cell viewWithTag:ITEM_DESCRIPTION_TAG];
        UILabel *itemQuantityLabel = (UILabel *)[cell viewWithTag:ITEM_QUANTITY_TAG];
        UILabel *crossLabel = (UILabel *)[cell viewWithTag:CROSS_LABEL_TAG];
        UILabel *itemPriceTotal = (UILabel *)[cell viewWithTag:ITEM_PRICE_TOTAL_TAG];
        UIStepper *itemQuantityStepper = (UIStepper *)[cell viewWithTag:ITEM_STEPPER_BUTTON_TAG];
        UIButton *addToCart = (UIButton *)[cell viewWithTag:ITEM_ADDTOCART_BUTTON_TAG];
        InventoryItem *item = [self.currentDisplayedInventoryItemArray objectAtIndex:[indexPath row]];//[self.inventoryItemArray objectAtIndex:[indexPath row]];
        cell.textLabel.text = [NSNumberFormatter localizedStringFromNumber:item.price numberStyle:NSNumberFormatterCurrencyStyle];
        itemName.text = item.name;
        itemDescription.text = item.itemDescription;
        [cell.textLabel setTextAlignment:UITextAlignmentRight];
        [cell.textLabel setBackgroundColor:[UIColor clearColor]];
        [cell.textLabel setFont:[UIFont  italicSystemFontOfSize:15]];

        // Update all the labels in the given cell.
        // Item in a cell are either added to the cart or not.
        if ([self.currentItemSelection objectForKey:itemName.text] == nil) {
            [addToCart setTitle:addToCartString forState:UIControlStateNormal];
            itemQuantityStepper.value = 0;
            itemQuantityLabel.text = @"";
            crossLabel.text = @"";
            itemPriceTotal.text = @"";
        } else {
            [addToCart setTitle:removeAllString forState:UIControlStateNormal];
            NSNumber *quantity = [self.currentItemSelection objectForKey:itemName.text];
            itemQuantityStepper.value = quantity.doubleValue;
            itemQuantityLabel.text = quantity.stringValue;
            crossLabel.text = crossLabelString;
            if (quantity.doubleValue > 0) {
                double newPrice = quantity.doubleValue * item.price.doubleValue;
                itemPriceTotal.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:newPrice] numberStyle:NSNumberFormatterCurrencyStyle];
            } else {
                itemPriceTotal.text = @"";
            }
        }
        if (clearAllSelectionData) { //reset all selected items to unselected
            [addToCart setTitle:addToCartString forState:UIControlStateNormal];
            itemQuantityStepper.value = 0;
            itemQuantityLabel.text = @"";
            itemPriceTotal.text = @"";
            crossLabel.text = @"";
            //get the index of the last item and set the "clearAllSelectionData" flag
            //to false because we're done resetting the table to initial state
            if (indexPath.row == [self.inventoryItemArray count]-1) { 
                clearAllSelectionData = NO;
            }
        }
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.categoriesTableview) {
        currentDisplayedCategory = [self.categoryArray objectAtIndex:indexPath.row];
        [self.currentDisplayedInventoryItemArray removeAllObjects];
        if ([currentDisplayedCategory isEqualToString:defaultCategoryName]) {
            self.currentDisplayedInventoryItemArray = [self.inventoryItemArray mutableCopy];
        } else {
            for (int i = 0; i < self.inventoryItemArray.count; i++) {
                InventoryItem *item = [self.inventoryItemArray objectAtIndex:i];
                if ([item.categoryName isEqualToString:currentDisplayedCategory]) {
                    [self.currentDisplayedInventoryItemArray addObject:item];
                }
            }
        }
        [self.inventoryItemsTableview reloadData];
    }
}

#pragma mark - Items view Control functions

- (IBAction)addToCartClicked:(UIButton *)sender
{
    //Get the superview from this button which will be our cell
    UITableViewCell *currentCell = (UITableViewCell*)[sender superview];
    UILabel *itemQuantityLabel = (UILabel *)[currentCell viewWithTag:ITEM_QUANTITY_TAG];
    UILabel *crossLabel = (UILabel *)[currentCell viewWithTag:CROSS_LABEL_TAG];
    UILabel *itemName = (UILabel *)[currentCell viewWithTag:ITEM_NAME_TAG];
    UILabel *itemPriceTotal = (UILabel *)[currentCell viewWithTag:ITEM_PRICE_TOTAL_TAG];
    UIStepper *itemQuantityStepper = (UIStepper *)[currentCell viewWithTag:ITEM_STEPPER_BUTTON_TAG];
    int itemIndex = [self array:self.currentDisplayedInventoryItemArray containsString:itemName.text];
    InventoryItem *item = [self.currentDisplayedInventoryItemArray objectAtIndex:itemIndex];
    if ([sender.currentTitle isEqualToString:addToCartString]) {
        [sender setTitle:removeAllString forState:UIControlStateNormal];
        itemQuantityStepper.value = 1;
        itemQuantityLabel.text = @"1";
        crossLabel.text = crossLabelString;
        itemPriceTotal.text = [NSNumberFormatter localizedStringFromNumber:item.price numberStyle:NSNumberFormatterCurrencyStyle];
        [self.currentItemSelection setObject:[NSNumber numberWithDouble:itemQuantityStepper.value] forKey:itemName.text];
        //do something with the value here
//        [self.delegate add:item toCart:self];
    } else {
        [sender setTitle:addToCartString forState:UIControlStateNormal];
        itemQuantityStepper.value = 0;
        itemQuantityLabel.text = @"";
        crossLabel.text = @"";
        [self.currentItemSelection removeObjectForKey:itemName.text];
        itemPriceTotal.text = @"";
//        [self.delegate remove:item fromCart:self];
//        int itemIndex = [self array:self.currentDisplayedInventoryItemArray containsString:itemName.text];
//        InventoryItem *item = [self.currentDisplayedInventoryItemArray objectAtIndex:itemIndex];
//        UITableViewCell *cell = [self.inventoryItemsTableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:itemIndex inSection:0]];
//        cell.textLabel.text = [NSNumberFormatter localizedStringFromNumber:item.price numberStyle:NSNumberFormatterCurrencyStyle];
    }
    [self updateSubtotalLabel];
}

- (IBAction)stepperClicked:(UIStepper *)sender
{    
    //Get the superview from this button which will be our cell
	UITableViewCell *currentCell = (UITableViewCell*)[sender superview];
    UILabel *itemQuantityLabel = (UILabel *)[currentCell viewWithTag:ITEM_QUANTITY_TAG];
    UILabel *itemName = (UILabel *)[currentCell viewWithTag:ITEM_NAME_TAG];
    UILabel *itemPriceTotal = (UILabel *)[currentCell viewWithTag:ITEM_PRICE_TOTAL_TAG];
    UILabel *crossLabel = (UILabel *)[currentCell viewWithTag:CROSS_LABEL_TAG];
    UIButton *addToCart = (UIButton *)[currentCell viewWithTag:ITEM_ADDTOCART_BUTTON_TAG];
    NSNumber *quantity = [NSNumber numberWithDouble:sender.value];
    int itemIndex = [self array:self.currentDisplayedInventoryItemArray containsString:itemName.text];
    InventoryItem *item = [self.currentDisplayedInventoryItemArray objectAtIndex:itemIndex];
        
    if ([quantity.stringValue isEqualToString:@"0"]) {
        [addToCart setTitle:addToCartString forState:UIControlStateNormal];
        itemQuantityLabel.text = @"";
        itemPriceTotal.text = @"";
        crossLabel.text = @"";
        [self.currentItemSelection removeObjectForKey:itemName.text];
//        [self.delegate remove:item fromCart:self];
    } else {// if user select more of this item, update the price
        double newPrice = item.price.doubleValue * quantity.doubleValue;
        itemPriceTotal.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:newPrice] numberStyle:NSNumberFormatterCurrencyStyle];
        [addToCart setTitle:removeAllString forState:UIControlStateNormal];
        itemQuantityLabel.text = [quantity stringValue];
        crossLabel.text = crossLabelString;
        [self.currentItemSelection setObject:quantity forKey:itemName.text];
    }
//    if (quantity.doubleValue > 0) {
//    }
//    UITableViewCell *cell = [self.inventoryItemsTableview cellForRowAtIndexPath:[NSIndexPath indexPathForRow:itemIndex inSection:0]];
//    cell.textLabel.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:newPrice] numberStyle:NSNumberFormatterCurrencyStyle];
    [self updateSubtotalLabel];
}

- (void)updateSubtotalLabel
{
    double subtotal = 0.0;
    for (NSString *key in self.currentItemSelection) {
        
        NSNumber *quantity = [self.currentItemSelection objectForKey:key];
        int itemIndex = [self array:self.inventoryItemArray containsString:key];
        InventoryItem *item = [self.inventoryItemArray objectAtIndex:itemIndex];
        subtotal += (item.price.doubleValue * quantity.doubleValue);
    }
    self.currentSubtotal = [NSNumber numberWithDouble:(self.itemsSubtotal.doubleValue + subtotal)];
    self.subtotalLabel.text = [NSNumberFormatter localizedStringFromNumber:self.currentSubtotal numberStyle:NSNumberFormatterCurrencyStyle];
    if (self.currentSubtotal.doubleValue == 0.0) {
        self.selectMethodOfPayment.enabled = NO;
    } else {
        self.selectMethodOfPayment.enabled = YES;
    }
}

- (IBAction)clearSelectionClicked:(UIBarButtonItem *)sender
{    
    clearAllSelectionData = YES;
//    self.itemsSubtotal = [NSNumber numberWithDouble:0.0];
//    self.currentSubtotal = [NSNumber numberWithDouble:0.0];
    [self.currentItemSelection removeAllObjects];
    [self.inventoryItemsTableview reloadData];
    [self updateSubtotalLabel];
}

- (void)setMenuStateSelected:(int)currentCheckoutMenuState
{
    // Get the master view controller in order to access the checkout menu state
    UIWindow *window = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] window];
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
    connectePOSMasterViewController *controller = (connectePOSMasterViewController *)masterNavigationController.topViewController;
    
    //set the current checkout menu state to the checkout initial state
    controller.currentCheckoutMenuState = [NSNumber numberWithInteger:currentCheckoutMenuState];
    //update the checkout menu state
    [controller.tableView reloadData];
}


#pragma mark - Items Search bar Control functions
   // called when text changes (including clear)

- (void)filterContentForSearchText:(NSString *)searchText
{
    [self.currentDisplayedInventoryItemArray removeAllObjects];
    [self.categoriesTableview deselectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.categoryArray indexOfObject:currentDisplayedCategory] inSection:0] animated:YES];
    if ([searchText isEqualToString:@""]) {
        [self.currentDisplayedInventoryItemArray addObjectsFromArray:self.inventoryItemArray];
        [self.inventoryItemsTableview reloadData];
        return;
    }
    NSString *attributeName = @"name"; //attribute to lookup in array object
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"%K contains[cd] %@", attributeName, searchText];
    [self.currentDisplayedInventoryItemArray addObjectsFromArray:[self.inventoryItemArray filteredArrayUsingPredicate:resultPredicate]];
    [self.inventoryItemsTableview reloadData];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self filterContentForSearchText:searchText];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self filterContentForSearchText:searchBar.text];
    [searchBar resignFirstResponder];
}

@end
