//
//  InventoryItem.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/23/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Item.h"


@interface InventoryItem : Item

@property (nonatomic, retain) NSString * categoryName;
@property (nonatomic, retain) NSString * itemDescription;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *inventoryItemCart;
@end

@interface InventoryItem (CoreDataGeneratedAccessors)

- (void)addInventoryItemCartObject:(NSManagedObject *)value;
- (void)removeInventoryItemCartObject:(NSManagedObject *)value;
- (void)addInventoryItemCart:(NSSet *)values;
- (void)removeInventoryItemCart:(NSSet *)values;

@end
