//
//  Item.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/11/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Item : NSManagedObject

@property (nonatomic, retain) NSNumber * isTaxable;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSManagedObject *itemCart;

@end
