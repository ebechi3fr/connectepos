//
//  connectePOSDetailViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "connectePOSItemsViewController.h"
#import "connectePOSMethodOfPaymentViewController.h"

@interface connectePOSDetailViewController : UIViewController <UISplitViewControllerDelegate, UIPopoverControllerDelegate, UITableViewDelegate, UITableViewDataSource, connectePOSItemsViewControllerDelegate>

//@property (strong, nonatomic) id detailItem;

//@property (strong, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UITableView *cartTableView;
@property (weak, nonatomic) IBOutlet UIToolbar *cartToolbar;
@property (weak, nonatomic) IBOutlet UITableView *transactionCostTableView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *selectMethodOfPayment;
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (void)selectTableRow:(NSInteger)r inSection:(NSInteger)s;
- (void)deselectTableRow:(NSInteger)r inSection:(NSInteger)s;
// used to add or to delete item from current transaction
- (void)addToCurrentTransaction:(double)itemPrice;
- (void)substractFromCurrentTransaction:(double)itemPrice;
- (void)setMenuStateSelected:(int)currentCheckoutMenuState;
@end
