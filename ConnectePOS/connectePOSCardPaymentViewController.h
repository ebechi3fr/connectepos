//
//  connectePOSCardPaymentViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface connectePOSCardPaymentViewController : UIViewController

- (void)setMenuStateSelected:(int)currentCheckoutMenuState;
@end
