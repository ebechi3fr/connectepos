//
//  connectePOSCashPaymentViewController.h
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface connectePOSCashPaymentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSNumber *total;
@property (strong, nonatomic) NSNumber *changeDue;
@property (weak, nonatomic) IBOutlet UILabel *display;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *cashOutButton;
@property (weak, nonatomic) IBOutlet UITableView *transactionTableView;
@property (weak, nonatomic) IBOutlet UIButton *exactAmountButton;
- (void)calculateAndDisplayChangeDue;
- (void)setMenuStateSelected:(int)currentCheckoutMenuState;
@end
