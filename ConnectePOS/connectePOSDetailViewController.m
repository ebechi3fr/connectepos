//
//  connectePOSDetailViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 7/30/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#include "ControlVariables.h"
#import "connectePOSDetailViewController.h"
#import "connectePOSMasterViewController.h"
#import "connectePOSAppDelegate.h"
#import "Cart.h"

@interface connectePOSDetailViewController ()
@property (strong, nonatomic) UIPopoverController *masterPopoverController;
@property (strong, nonatomic) UIBarButtonItem *barButtonItem; //Button to access Main menu options
@property (strong, nonatomic) UIViewController *masterViewController; //Main menu view controller
@property (strong, nonatomic) NSNumberFormatter *numberFormatter; //allows numbers to be properly formated for display
@property (strong, nonatomic) NSMutableArray *cartItems;
@property (strong, nonatomic) NSMutableArray* selectedInventoryItems; //All the inventory items
@property (strong, nonatomic) NSMutableDictionary* selectedInventoryItemsAndQuantity; //A Dictionary of the selected inventory items in current transaction  and the subtotal price for each of them

//@property (strong, nonatomic) Cart *shoppingCart;               //current transaction cart
- (void)configureView;
@end

@implementation connectePOSDetailViewController

//@synthesize detailItem = _detailItem;
//@synthesize detailDescriptionLabel = _detailDescriptionLabel;
@synthesize cartTableView = _cartTableView;
@synthesize cartToolbar = _cartToolbar;
@synthesize transactionCostTableView = _transactionCostTableView;
@synthesize selectMethodOfPayment = _selectMethodOfPayment;
@synthesize display = _display;
@synthesize masterPopoverController = _masterPopoverController;
@synthesize barButtonItem = _barButtonItem;
@synthesize masterViewController = _masterViewController;
@synthesize numberFormatter = _numberFormatter;
@synthesize cartItems = _cartItems;
@synthesize selectedInventoryItems = _selectedInventoryItems;
@synthesize selectedInventoryItemsAndQuantity = _selectedInventoryItemsAndQuantity;
//@dynamic shoppingCart;

Cart *shoppingCart;               //current transaction cart
bool inTheMiddleOfEnterringANumber = NO;
double currentNumber = 0.00;
double runningTotal = 0.00;
double currentTaxes = 0.00;
double currentSubtotal = 0.00;
NSInteger numberOfRowsInTransactionCostTable = INITIAL_NUM_ROWS;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
//    if (_detailItem != newDetailItem) {
//        _detailItem = newDetailItem;
        
        // Update the view.
//        [self configureView];
//    }

//    if (self.masterPopoverController != nil) {
//        [self.masterPopoverController dismissPopoverAnimated:YES];
//    }
}

- (void)configureView
{
    // Update the user interface for the detail item.

//    if (self.detailItem) {
//        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.masterPopoverController.delegate = self;
    self.cartTableView.delegate = self;
    self.cartTableView.dataSource = self;
    self.transactionCostTableView.delegate = self;
    self.transactionCostTableView.dataSource =self;
    
    // insert the table edit button on the toolbar
    NSMutableArray *newToolbarItems = [self.cartToolbar.items mutableCopy];
    [newToolbarItems insertObject:self.editButtonItem atIndex:0];
    self.cartToolbar.items = newToolbarItems;
    
    // initialize number formatter used to display numbers in dollar currency
    self.numberFormatter = [[NSNumberFormatter alloc] init];
    self.cartItems = [[NSMutableArray alloc] init];//WithObjects:[NSNumber numberWithDouble:0.0], nil];
    self.selectedInventoryItemsAndQuantity = [[NSMutableDictionary alloc] init];
    self.selectedInventoryItems = [[NSMutableArray alloc] init];
    if (self.managedObjectContext == nil) {
        self.managedObjectContext = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] managedObjectContext];
                NSLog(@"After managedObjectContext: %@",  self.managedObjectContext);
    }

    shoppingCart = (Cart *)[NSEntityDescription insertNewObjectForEntityForName:@"Cart" inManagedObjectContext:self.managedObjectContext];
    
    [self.numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    // specify positive, zero, and negative formats
    [self.numberFormatter setPositiveFormat:@"$#,##0.00"];
    [self.numberFormatter setNegativeFormat:@"($#,##)0.00"];
    [self.numberFormatter setZeroSymbol:@"$0.00"];
    // number formater for display
    
    [self configureView];
}

- (void)viewDidUnload
{
    [self setCartTableView:nil];
    [self setCartToolbar:nil];
    [self setDisplay:nil];
    [self setTransactionCostTableView:nil];
    [self setSelectMethodOfPayment:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
//    self.detailDescriptionLabel = nil;
    self.cartItems = nil;
    self.selectedInventoryItems = nil;
    self.selectedInventoryItemsAndQuantity = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    // select current checkout menu state on the master view
    [self setMenuStateSelected:CHECKOUT_START_STATE];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)setMenuStateSelected:(int)currentCheckoutMenuState
{
    // Get the master view controller in order to access the checkout menu state
    UIWindow *window = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] window];
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
    connectePOSMasterViewController *controller = (connectePOSMasterViewController *)masterNavigationController.topViewController;
    
    //set the current checkout menu state to the checkout initial state
    controller.currentCheckoutMenuState = [NSNumber numberWithInteger:currentCheckoutMenuState];
    //update the checkout menu state
    [controller.tableView reloadData];
}

#pragma mark - Split view

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
    self.barButtonItem = barButtonItem;
    barButtonItem.title = viewController.title;//NSLocalizedString(@"Master", @"Master");
    [self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
    self.masterPopoverController = popoverController;
    self.masterViewController = viewController;
//    NSLog(@"a popover was dismissed! thank you stackoverflow!");
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
    // Called when the view is shown again in the split view, invalidating the button and popover controller.
    [self.navigationItem setLeftBarButtonItem:nil animated:YES];
    self.barButtonItem = nil;
    self.masterPopoverController = nil;
    self.masterViewController = nil;
}

//- (void)splitViewController:(UISplitViewController *)svc popoverController:(UIPopoverController *)pc willPresentViewController:(UIViewController *)aViewController
//{
//    self.barButtonItem.title = aViewController.title;
//}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return DEFAULT_NUM_TABLE_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.cartTableView) {
        return [self.cartItems count];
    } else if (tableView == self.transactionCostTableView) {
        return numberOfRowsInTransactionCostTable;// tax and subtotal cells
    } else {
        return INITIAL_NUM_ROWS;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    UITableViewCell *cell;
    
    if (tableView == self.cartTableView) {
        cellIdentifier = @"ItemCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }

        UILabel *itemName = (UILabel *)[cell viewWithTag:ITEM_NAME_TAG];
        UILabel *itemDescription = (UILabel *)[cell viewWithTag:ITEM_DESCRIPTION_TAG];
        InventoryItem *inventoryItem;
        Item *item;
        NSNumber *itemQuantity;
        NSNumber *itemPrice;
        id object = [self.cartItems objectAtIndex:indexPath.row];
        if ([object isKindOfClass:[InventoryItem class]]) {
            inventoryItem = [self.cartItems objectAtIndex:indexPath.row];
            itemQuantity = [self.selectedInventoryItemsAndQuantity objectForKey:inventoryItem.name];
            itemPrice = [NSNumber numberWithDouble:inventoryItem.price.doubleValue * itemQuantity.doubleValue];
            if (itemQuantity.doubleValue > 1) {
                itemName.text = [inventoryItem.name stringByAppendingFormat:@"      %@ x $%@", itemQuantity.stringValue, inventoryItem.price.stringValue];
            } else {
                itemName.text = inventoryItem.name;                
            }
            itemDescription.text = inventoryItem.itemDescription;
        } else {
            item = [self.cartItems objectAtIndex:indexPath.row];
            itemPrice = item.price;
            itemName.text = @"Item";
            itemDescription.text = @"Item description...";
        }

        cell.textLabel.text = [NSNumberFormatter localizedStringFromNumber:itemPrice numberStyle:NSNumberFormatterCurrencyStyle];
        if (itemPrice.doubleValue == 0.00) {
            //Make sure not to print out name and detail description of item when price is $0.00
            itemName.text = @"";
            itemDescription.text = @"";
        }
        
   } else if (tableView == self.transactionCostTableView) {
        cellIdentifier = @"transactionCostCell";
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        // Configure the cell...
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
//            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
        }
       UILabel *itemName = (UILabel *)[cell viewWithTag:SUBTOTAL_TAX_TAG];//[[UILabel alloc] initWithFrame:rect];
        if ([indexPath row] == 0) { //Subtotal row
            itemName.text = @"Subtotal";
            cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentSubtotal] numberStyle:NSNumberFormatterCurrencyStyle];
        } else if ([indexPath row] == 1) { //Tax row
            itemName.text = @"6% Sales Tax";
            cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentTaxes] numberStyle:NSNumberFormatterCurrencyStyle];
        } else {
            itemName.text = @"Total";
            cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];
        }
//        NSLog(@"Inside transaction cell table view");
    }
    [cell.textLabel setTextAlignment:UITextAlignmentRight];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    if (tableView == self.cartTableView) {
        if (indexPath.row == [self.cartItems indexOfObject:self.cartItems.lastObject]) {
            return NO;
        }
        return YES;
    } else if (tableView == self.transactionCostTableView) {
        return NO;
    }
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    if (editingStyle == UITableViewCellEditingStyleDelete) {
    //        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    //        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    //
    //        NSError *error = nil;
    //        if (![context save:&error]) {
    //             // Replace this implementation with code to handle the error appropriately.
    //             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
    //            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    //            abort();
    //        }
    //    }
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if (tableView == self.cartTableView) {
            InventoryItem *inventoryItem;
            Item *item;
            NSNumber *itemPriceToRemove;
            NSNumber *itemQuantity;
            id object = [self.cartItems objectAtIndex:indexPath.row];
            if ([object isKindOfClass:[InventoryItem class]]) {
                inventoryItem = [self.cartItems objectAtIndex:indexPath.row];
                itemQuantity = [self.selectedInventoryItemsAndQuantity objectForKey:inventoryItem.name];
                itemPriceToRemove = [NSNumber numberWithDouble:inventoryItem.price.doubleValue * itemQuantity.doubleValue];
                [self.selectedInventoryItems removeObject:inventoryItem];
                [self.selectedInventoryItemsAndQuantity removeObjectForKey:inventoryItem.name];
           } else {
                item = [self.cartItems objectAtIndex:indexPath.row];
                itemPriceToRemove = item.price;
           }
            [self substractFromCurrentTransaction:itemPriceToRemove.doubleValue]; // pass a negative number to update current transaction to remove that number
            NSLog(@"price to remove: %f", itemPriceToRemove.doubleValue);
            NSLog(@"current subtotal: %f", currentSubtotal);
            [self.cartItems removeObjectAtIndex:indexPath.row];
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            inTheMiddleOfEnterringANumber = NO;
            if ([self.cartItems count] == 0) {
                numberOfRowsInTransactionCostTable = INITIAL_NUM_ROWS;
            }
            // update the transaction table view and the total cost view
            [self.transactionCostTableView reloadData];
            self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];
            if (runningTotal == 0.0) {
                self.selectMethodOfPayment.enabled = NO;
            } else {
                self.selectMethodOfPayment.enabled = YES;
            }
        }
    }
}

- (void)selectTableRow:(NSInteger)r inSection:(NSInteger)s
{   //sets the first row in the table menu to be selected
    [self.cartTableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s]  animated:NO scrollPosition:UITableViewScrollPositionNone];
    //    [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
}

- (void)deselectTableRow:(NSInteger)r inSection:(NSInteger)s
{
    [self.cartTableView deselectRowAtIndexPath:[NSIndexPath indexPathForRow:r inSection:s] animated:NO];
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // The table view should not be re-orderable.
    return NO;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    NSManagedObject *object = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    //    self.detailViewController.detailItem = object;
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    [self.cartTableView setEditing:editing animated:YES];
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath;
{
}

#pragma mark - PopOver view

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.barButtonItem.title = self.masterViewController.title;
    NSLog(@"a popover was dismissed! thank you stackoverflow!");
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ItemsViewControllerSegue"]) {
        [segue.destinationViewController setDelegate:self];
        inTheMiddleOfEnterringANumber = NO; //Not enterring number when switching to a different view.
        connectePOSItemsViewController *vc = (connectePOSItemsViewController *) segue.destinationViewController;
        vc.currentItemSelection = [[NSMutableDictionary alloc] initWithDictionary:self.selectedInventoryItemsAndQuantity];
        //used to sync subtotal label view on items page the first time it's displayed
        vc.currentSubtotal = [[NSNumber alloc] initWithDouble:currentSubtotal];
        
        vc.itemsSubtotal = [[NSNumber alloc] initWithDouble:currentSubtotal];
        for (int i = 0; i < self.cartItems.count; i++) {
            InventoryItem *inventoryItem;
            NSNumber *itemQuantity;
            id object = [self.cartItems objectAtIndex:i];
            if ([object isKindOfClass:[InventoryItem class]]) {
                inventoryItem = [self.cartItems objectAtIndex:i];
                itemQuantity = [self.selectedInventoryItemsAndQuantity objectForKey:inventoryItem.name];
                vc.itemsSubtotal =  [NSNumber numberWithDouble:(vc.itemsSubtotal.doubleValue - (inventoryItem.price.doubleValue * itemQuantity.doubleValue))];
            }
        }
    } else if ([segue.identifier isEqualToString:@"SelectPaymentViewControllerSegue"]) {
        NSLog(@"Selecting Method of payment segue");
        connectePOSMethodOfPaymentViewController *vc = (connectePOSMethodOfPaymentViewController *) segue.destinationViewController;
        vc.subtotal = [[NSNumber alloc] initWithDouble:currentSubtotal];
        vc.taxes = [[NSNumber alloc] initWithDouble:currentTaxes];
        vc.total = [[NSNumber alloc] initWithDouble:runningTotal];
    }
}


#pragma mark - Calculation Controlls

- (IBAction)digitPressed:(UIButton *)sender {
    if (!inTheMiddleOfEnterringANumber) {
        if ([self.cartItems count] == 0) {
            Item *item = (Item *)[NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext];
            item.price = [NSNumber numberWithDouble:0.0];
            item.isTaxable = [NSNumber numberWithBool:YES];
            item.itemCart = shoppingCart;
            NSLog(@"Item isTaxable %d", item.isTaxable.boolValue);
            [self.cartItems addObject:item]; //initialize the table
        }
        numberOfRowsInTransactionCostTable = NUM_ROWS_DURING_TRANSACTION;
        [self.cartTableView reloadData];
        [self.transactionCostTableView reloadData];
        [self selectTableRow:[self.cartItems indexOfObject:self.cartItems.lastObject] inSection:0];
        inTheMiddleOfEnterringANumber = YES;
    }
    NSString *digit = [sender currentTitle];
    NSLog(@"digit pressed: %@", digit);
    // Convert the digit pressed into cents
    double digitInCents = [digit doubleValue] * UNIT_IN_CENTS;
    
    // get the current number enterred so far from the display
//    runningTotal = [[self.numberFormatter numberFromString:self.display.text] doubleValue];//[self.display.text doubleValue];
    currentNumber = [[self.numberFormatter numberFromString:[self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text] doubleValue]; //gets the textlabel of the selected row
    //[[self.numberFormatter numberFromString:self.display.text] doubleValue];//[self.display.text doubleValue];
    
    
    // shift running total decimal to the right
//    runningTotal *= DECIMAL_SHIFTER_CONST;
    currentNumber *= DECIMAL_SHIFTER_CONST;
    
    if ([digit isEqualToString:@"00"]) {
//        runningTotal *= DECIMAL_SHIFTER_CONST;
        currentNumber *= DECIMAL_SHIFTER_CONST;
    }
    
    // add the digit pressed to the running total
//    runningTotal += digitInCents;
    currentNumber += digitInCents;
    
    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentNumber] numberStyle:NSNumberFormatterCurrencyStyle];
//    self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];//[NSString stringWithFormat: @"%f", runningTotal];
}

- (void)addToCurrentTransaction:(double)itemPrice
{
    if (itemPrice == 0.0) {
        return;
    }
    
    // calculate the current transaction tax
    currentTaxes += itemPrice * MICHIGAN_TAX;
    
    // calculate the current subtotal
    currentSubtotal += itemPrice;
    
    // calculate the running total
    runningTotal = currentSubtotal + currentTaxes; //currentNumber;
}

- (void)substractFromCurrentTransaction:(double)itemPrice
{
    if (itemPrice == 0.0) {
        return;
    }
    
    // calculate the current transaction tax
    currentTaxes -= itemPrice * MICHIGAN_TAX;
    if (currentTaxes < 0.0) currentTaxes = 0.0;
    // calculate the current subtotal
    currentSubtotal -= itemPrice;
    if (currentSubtotal < 0.0) currentSubtotal = 0.0;
    // calculate the running total
    runningTotal = currentSubtotal + currentTaxes; //currentNumber;    
}

- (IBAction)deletePressed {
    if (!inTheMiddleOfEnterringANumber) {
        return;
    }
    currentNumber = [[self.numberFormatter numberFromString:[self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text] doubleValue]; //gets the textlabel of the selected row

    // shift current number decimal to the left
    NSNumberFormatter *deleteFormatter = [[NSNumberFormatter alloc] init];
    [deleteFormatter setRoundingMode:NSNumberFormatterRoundDown];
    [deleteFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [deleteFormatter setPositiveFormat:@"$#,##0.00"];
    [deleteFormatter setNegativeFormat:@"($#,##)0.00"];
    [deleteFormatter setZeroSymbol:@"$0.00"];

    currentNumber /= DECIMAL_SHIFTER_CONST;
    NSLog(@"delete: %f", currentNumber);
    if (currentNumber < 0.01) {
        currentNumber = 0.0;
        NSLog(@"delete after format: %f", currentNumber);
    }
//    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentNumber] numberStyle:NSNumberFormatterCurrencyStyle];
    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text = [deleteFormatter stringFromNumber:[NSNumber numberWithDouble:currentNumber]];
}

- (IBAction)addPressed {
    NSLog(@"Number of items before adding: %d", self.cartItems.count);
    
    //gets the textlabel of the selected row
    currentNumber = [[self.numberFormatter numberFromString:[self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text] doubleValue];
    
    if (currentNumber == 0.0) {
        return;
    }
    [self addToCurrentTransaction:currentNumber];
    
   
    // create new item to be added to current shopping cart
    Item *item = (Item *)[NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext];
    item.price = [NSNumber numberWithDouble:currentNumber];
    item.isTaxable = [NSNumber numberWithBool:YES];
    item.itemCart = shoppingCart;
    // add to current cart
    
    NSLog(@"add: %f", currentNumber);
    // insert the new item into the current transaction array of items
//    [self.cartItems replaceObjectAtIndex:[self.cartItems indexOfObject:self.cartItems.lastObject] withObject:[NSNumber numberWithDouble:currentNumber]];
    [self.cartItems insertObject:item atIndex:[self.cartItems indexOfObject:self.cartItems.lastObject]];
    [self deselectTableRow:[self.cartItems indexOfObject:self.cartItems.lastObject] inSection:0];
    // reload the tables because transaction array of items has been updated
    [self.cartTableView reloadData];
    [self.transactionCostTableView reloadData];
    currentNumber = 0.00;
    inTheMiddleOfEnterringANumber = NO;
    // update the display
    self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];//[NSString stringWithFormat: @"%f", runningTotal];
    self.selectMethodOfPayment.enabled = YES; //enable the select payment button
    [self.cartTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.cartItems indexOfObject:self.cartItems.lastObject] inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    NSLog(@"Number of items after adding: %d", self.cartItems.count);
}

- (IBAction)clearOperation:(id)sender {
    runningTotal = 0.00;
    currentNumber = 0.00;
    currentTaxes = 0.00;
    currentSubtotal = 0.00;
    inTheMiddleOfEnterringANumber = NO;
    // clear the current table too
    
    // update the display
    self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];
    self.selectMethodOfPayment.enabled = NO;
//    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]].textLabel.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:currentNumber] numberStyle:NSNumberFormatterCurrencyStyle];
//    [self.cartTableView cellForRowAtIndexPath:[self.cartTableView indexPathForSelectedRow]]
//    [self deselectTableRow:[self.cartItems indexOfObject:self.cartItems.lastObject] inSection:0];
    
    // clear the transaction items array and update the table view
    [self.cartItems removeAllObjects];
    [self.selectedInventoryItemsAndQuantity removeAllObjects];
    [self.selectedInventoryItems removeAllObjects];
    numberOfRowsInTransactionCostTable = INITIAL_NUM_ROWS;
//    self.cartItems = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithDouble:0.0], nil];
    //reload tables because table data has been updated
    [self.cartTableView reloadData];
    [self.transactionCostTableView reloadData]; 
}

- (void)updateCart:(connectePOSItemsViewController *)sender With:(NSMutableDictionary *)selectedInventoryItems from:(NSArray *)inventoryItems
{
    for (int i = 0; i < self.cartItems.count;) {
        InventoryItem *inventoryItem;
        NSNumber *itemQuantity;
        id object = [self.cartItems objectAtIndex:i];
        if ([object isKindOfClass:[InventoryItem class]]) {
            inventoryItem = [self.cartItems objectAtIndex:i];
            itemQuantity = [self.selectedInventoryItemsAndQuantity objectForKey:inventoryItem.name];
            [self substractFromCurrentTransaction:(inventoryItem.price.doubleValue * itemQuantity.doubleValue)];
            [self.cartItems removeObjectAtIndex:i];
            i = 0; //reset the counter because we modified the number of object in the array
            NSLog(@"name is: %@", inventoryItem.name);
        } else {
            i++;
        }
    }
    [self.selectedInventoryItemsAndQuantity removeAllObjects];
    [self.selectedInventoryItems removeAllObjects];
    for (NSString* key in selectedInventoryItems) {
        //add selected inventory item to current cartItems
        NSNumber *quantity = [selectedInventoryItems objectForKey:key];
        int itemIndex = [sender array:inventoryItems containsString:key];
        InventoryItem *inventoryItem = [inventoryItems objectAtIndex:itemIndex];
        if ([self.cartItems count] == 0) {
            Item *item = (Item *)[NSEntityDescription insertNewObjectForEntityForName:@"Item" inManagedObjectContext:self.managedObjectContext];
            item.price = [NSNumber numberWithDouble:0.0];
            item.isTaxable = [NSNumber numberWithBool:YES];
            item.itemCart = shoppingCart;
            [self.cartItems addObject:item]; //initialize the table
            numberOfRowsInTransactionCostTable = NUM_ROWS_DURING_TRANSACTION;
        }
        [self.cartItems insertObject:inventoryItem atIndex:[self.cartItems indexOfObject:self.cartItems.lastObject]];
        double newPrice = inventoryItem.price.doubleValue * quantity.doubleValue;
        [self addToCurrentTransaction:newPrice];
        [self.selectedInventoryItems addObject:inventoryItem];
        [self.selectedInventoryItemsAndQuantity setObject:quantity forKey:key];
    }
    [self.cartTableView reloadData];
    [self.transactionCostTableView reloadData];
    self.display.text = [NSNumberFormatter localizedStringFromNumber:[NSNumber numberWithDouble:runningTotal] numberStyle:NSNumberFormatterCurrencyStyle];
    if (runningTotal == 0.0) {
        self.selectMethodOfPayment.enabled = NO;
    } else {
        self.selectMethodOfPayment.enabled = YES;
    }
}

@end
