//
//  connectePOSCardPaymentViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSCardPaymentViewController.h"
#include "ControlVariables.h"
#import "connectePOSMasterViewController.h"
#import "connectePOSAppDelegate.h"

@interface connectePOSCardPaymentViewController ()

@end
//card payment is next
@implementation connectePOSCardPaymentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    // select current checkout menu state on the master view
    [self setMenuStateSelected:CHECKOUT_METHOD_OF_PAYMENT_STATE];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)setMenuStateSelected:(int)currentCheckoutMenuState
{
    // Get the master view controller in order to access the checkout menu state
    UIWindow *window = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] window];
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
    connectePOSMasterViewController *controller = (connectePOSMasterViewController *)masterNavigationController.topViewController;
    
    //set the current checkout menu state to the checkout initial state
    controller.currentCheckoutMenuState = [NSNumber numberWithInteger:currentCheckoutMenuState];
    //update the checkout menu state
    [controller.tableView reloadData];
}


@end
