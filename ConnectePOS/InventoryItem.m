//
//  InventoryItem.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 8/23/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "InventoryItem.h"


@implementation InventoryItem

@dynamic categoryName;
@dynamic itemDescription;
@dynamic name;
@dynamic inventoryItemCart;

@end
