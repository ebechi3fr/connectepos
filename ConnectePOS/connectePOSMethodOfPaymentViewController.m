//
//  connectePOSMethodOfPaymentViewController.m
//  ConnectePOS
//
//  Created by Emeric Bechi on 9/5/12.
//  Copyright (c) 2012 Sourceconnecte. All rights reserved.
//

#import "connectePOSMethodOfPaymentViewController.h"
#import "connectePOSCashPaymentViewController.h"
#include "ControlVariables.h"
#import "connectePOSMasterViewController.h"
#import "connectePOSAppDelegate.h"

@interface connectePOSMethodOfPaymentViewController ()

@end

@implementation connectePOSMethodOfPaymentViewController
@synthesize transactionCostTableView = _transactionCostTableView;

NSInteger numberOfRowsInPaymentMethodTable = 3;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.transactionCostTableView.dataSource = self;
    self.transactionCostTableView.delegate = self;
}

- (void)viewDidUnload
{
    [self setTransactionCostTableView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    // select current checkout menu state on the master view
    [self setMenuStateSelected:CHECKOUT_METHOD_OF_PAYMENT_STATE];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)setMenuStateSelected:(int)currentCheckoutMenuState
{
    // Get the master view controller in order to access the checkout menu state
    UIWindow *window = [(connectePOSAppDelegate *)[[UIApplication sharedApplication]delegate] window];
    UISplitViewController *splitViewController = (UISplitViewController *)window.rootViewController;
    UINavigationController *masterNavigationController = [splitViewController.viewControllers objectAtIndex:0];
    connectePOSMasterViewController *controller = (connectePOSMasterViewController *)masterNavigationController.topViewController;
    
    //set the current checkout menu state to the checkout initial state
    controller.currentCheckoutMenuState = [NSNumber numberWithInteger:currentCheckoutMenuState];
    //update the checkout menu state
    [controller.tableView reloadData];
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return DEFAULT_NUM_TABLE_SECTIONS;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return numberOfRowsInPaymentMethodTable;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier;
    UITableViewCell *cell;

    
    cellIdentifier = @"paymentMethodCell";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        //            cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    }
    UILabel *transactionCostLabel = (UILabel *)[cell viewWithTag:SUBTOTAL_TAX_TAG];
    if ([indexPath row] == 0) { //Subtotal row
        transactionCostLabel.text = @"Subtotal";
        cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:self.subtotal numberStyle:NSNumberFormatterCurrencyStyle];
    } else if ([indexPath row] == 1) { //Tax row
        transactionCostLabel.text = @"6% Sales Tax";
        cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:self.taxes numberStyle:NSNumberFormatterCurrencyStyle];
    } else {
        transactionCostLabel.text = @"Total";
        cell.textLabel.text =[NSNumberFormatter localizedStringFromNumber:self.total numberStyle:NSNumberFormatterCurrencyStyle];
    }
    
    [cell.textLabel setTextAlignment:UITextAlignmentRight];
    [cell.textLabel setBackgroundColor:[UIColor clearColor]];
    return cell;
}

#pragma mark - Prepare for segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"CashPaymentViewControllerSegue"]) {
        NSLog(@"Selecting Cash payment segue");
        connectePOSCashPaymentViewController *vc = (connectePOSCashPaymentViewController *) segue.destinationViewController;
        vc.total = self.total;
    }    
}

@end
